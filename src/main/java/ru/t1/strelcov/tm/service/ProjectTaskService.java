package ru.t1.strelcov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.repository.IProjectRepository;
import ru.t1.strelcov.tm.api.repository.ITaskRepository;
import ru.t1.strelcov.tm.api.service.IProjectTaskService;
import ru.t1.strelcov.tm.exception.empty.EmptyIdException;
import ru.t1.strelcov.tm.exception.entity.AccessDeniedException;
import ru.t1.strelcov.tm.exception.entity.EmptyProjectIdException;
import ru.t1.strelcov.tm.exception.entity.EmptyTaskIdException;
import ru.t1.strelcov.tm.exception.entity.EntityNotFoundException;
import ru.t1.strelcov.tm.model.Project;
import ru.t1.strelcov.tm.model.Task;

import java.util.List;
import java.util.Optional;

public final class ProjectTaskService implements IProjectTaskService {

    @NotNull
    private final ITaskRepository taskRepository;

    @NotNull
    private final IProjectRepository projectRepository;

    public ProjectTaskService(@NotNull final IProjectRepository projectRepository, @NotNull final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
    }

    @NotNull
    @Override
    public List<Task> findAllTasksByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(projectId).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        @NotNull final List<Task> tasksOfProject = taskRepository.findAllByProjectId(userId, projectId);
        return tasksOfProject;
    }

    @NotNull
    @Override
    public Task bindTaskToProject(@Nullable final String userId, @Nullable final String taskId, @Nullable final String projectId) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(taskId).filter((i) -> !i.isEmpty()).orElseThrow(EmptyTaskIdException::new);
        Optional.ofNullable(projectId).filter((i) -> !i.isEmpty()).orElseThrow(EmptyTaskIdException::new);
        @NotNull final Task task = Optional.ofNullable(taskRepository.findById(userId, taskId)).orElseThrow(EntityNotFoundException::new);
        task.setProjectId(projectId);
        return task;
    }

    @NotNull
    @Override
    public Task unbindTaskFromProject(@Nullable final String userId, @Nullable final String taskId) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(taskId).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        @NotNull final Task task = Optional.ofNullable(taskRepository.findById(userId, taskId)).orElseThrow(EntityNotFoundException::new);
        task.setProjectId(null);
        return task;
    }

    @NotNull
    @Override
    public Project removeProjectById(@Nullable final String userId, @Nullable final String projectId) {
        Optional.ofNullable(userId).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(projectId).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        taskRepository.removeAllByProjectId(userId, projectId);
        @NotNull final Project project = Optional.ofNullable(projectRepository.removeById(userId, projectId)).orElseThrow(EntityNotFoundException::new);
        return project;
    }

}
