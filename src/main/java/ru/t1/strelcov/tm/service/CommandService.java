package ru.t1.strelcov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.repository.ICommandRepository;
import ru.t1.strelcov.tm.api.service.ICommandService;
import ru.t1.strelcov.tm.command.AbstractCommand;
import ru.t1.strelcov.tm.exception.system.CorruptCommandException;
import ru.t1.strelcov.tm.exception.system.UnknownCommandException;

import java.util.Collection;
import java.util.Optional;

public final class CommandService implements ICommandService {

    @NotNull
    private final ICommandRepository commandRepository;

    public CommandService(@NotNull ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }

    @Override
    public void add(@Nullable final AbstractCommand command) {
        Optional.ofNullable(command).map(AbstractCommand::name).filter((i) -> !i.isEmpty()).orElseThrow(CorruptCommandException::new);
        commandRepository.add(command);
    }

    @NotNull
    @Override
    public AbstractCommand getCommandByName(@NotNull final String name) {
        return Optional.ofNullable(commandRepository.getCommandByName(name)).orElseThrow(() -> new UnknownCommandException(name));
    }

    @NotNull
    @Override
    public AbstractCommand getCommandByArg(@NotNull final String arg) {
        return Optional.ofNullable(commandRepository.getCommandByArg(arg)).orElseThrow(() -> new UnknownCommandException(arg));
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getArguments() {
        return commandRepository.getArguments();
    }

}
