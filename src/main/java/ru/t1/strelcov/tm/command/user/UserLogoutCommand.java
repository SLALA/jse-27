package ru.t1.strelcov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.service.IAuthService;

public final class UserLogoutCommand extends AbstractUserCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "logout";
    }

    @NotNull
    @Override
    public String description() {
        return "Logout.";
    }

    @Override
    public void execute() {
        @NotNull final IAuthService authService = serviceLocator.getAuthService();
        System.out.println("[LOGOUT]");
        authService.logout();
    }

}
