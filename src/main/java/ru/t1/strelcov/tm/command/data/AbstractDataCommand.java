package ru.t1.strelcov.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.command.AbstractCommand;
import ru.t1.strelcov.tm.dto.Domain;

public abstract class AbstractDataCommand extends AbstractCommand {

    @NotNull
    protected static String FILE_BINARY = "./data.bin";
    @NotNull
    protected static String FILE_BASE64 = "./data.base64";

    @NotNull
    protected Domain getDomain() {
        @NotNull final Domain domain = new Domain();
        domain.setUsers(serviceLocator.getUserService().findAll());
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        return domain;
    }

    protected void setDomain(@Nullable Domain domain) {
        if (domain == null) return;
        serviceLocator.getUserService().clear();
        serviceLocator.getUserService().addAll(domain.getUsers());
        serviceLocator.getProjectService().clear();
        serviceLocator.getProjectService().addAll(domain.getProjects());
        serviceLocator.getTaskService().clear();
        serviceLocator.getTaskService().addAll(domain.getTasks());
        serviceLocator.getAuthService().logout();
        System.out.println("Automatic logout occurred. To continue init login command.");
    }

}
