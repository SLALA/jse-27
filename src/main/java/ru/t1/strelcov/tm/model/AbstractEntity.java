package ru.t1.strelcov.tm.model;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.UUID;

@Getter
public abstract class AbstractEntity implements Serializable {

    @NotNull
    protected final String id = UUID.randomUUID().toString();

}
