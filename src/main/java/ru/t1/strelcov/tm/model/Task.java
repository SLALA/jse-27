package ru.t1.strelcov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public final class Task extends AbstractBusinessEntity {
    @Nullable
    private String projectId;

    public Task(@Nullable String userId, @Nullable String name) {
        super(userId, name);
    }

    public Task(@Nullable String userId, @Nullable String name, @Nullable String description) {
        super(userId, name, description);
    }

}
