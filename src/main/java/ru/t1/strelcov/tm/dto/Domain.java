package ru.t1.strelcov.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.model.Project;
import ru.t1.strelcov.tm.model.Task;
import ru.t1.strelcov.tm.model.User;

import java.io.Serializable;
import java.util.List;

@NoArgsConstructor
@Setter
@Getter
public class Domain implements Serializable {

    @Nullable
    private List<User> users;

    @Nullable
    private List<Task> tasks;

    @Nullable
    private List<Project> projects;

}
