package ru.t1.strelcov.tm.api;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    @NotNull
    Integer getPasswordIteration();

    @NotNull
    String getPasswordSecret();

    @NotNull
    String getName();

    @NotNull
    String getVersion();

    @NotNull
    String getEmail();

}
